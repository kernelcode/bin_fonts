/* Small, 8px high font */

#ifndef TINY_FONT_H
#define TINY_FONT_H

#include "charset_t.h"

#define TINY_FONT_NUMCHARS  96
#define TINY_FONT_FIRST     ' '
#define TINY_FONT_HEIGHT    1

unsigned char tiny_font_bytes[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x74, 0x00, 0x60, 0x00, 0x60, 
    0x00, 0x28, 0x7C, 0x28, 0x7C, 0x28, 0x00, 0x14, 0x34, 0x6E, 
    0x28, 0x00, 0x40, 0x0C, 0x10, 0x60, 0x04, 0x00, 0x28, 0x54, 
    0x54, 0x08, 0x14, 0x00, 0x60, 0x00, 0x38, 0x44, 0x00, 0x44, 
    0x38, 0x00, 0x50, 0x20, 0x50, 0x00, 0x10, 0x38, 0x10, 0x00, 
    0x02, 0x04, 0x00, 0x10, 0x10, 0x10, 0x00, 0x04, 0x00, 0x04, 
    0x08, 0x10, 0x20, 0x40, 0x00, 0x38, 0x44, 0x44, 0x38, 0x00, 
    0x40, 0x7C, 0x00, 0x4C, 0x54, 0x54, 0x24, 0x00, 0x44, 0x54, 
    0x54, 0x28, 0x00, 0x18, 0x28, 0x7C, 0x08, 0x00, 0x74, 0x54, 
    0x54, 0x48, 0x00, 0x38, 0x54, 0x54, 0x08, 0x00, 0x40, 0x4C, 
    0x50, 0x60, 0x00, 0x28, 0x54, 0x54, 0x28, 0x00, 0x20, 0x54, 
    0x54, 0x38, 0x00, 0x28, 0x00, 0x2C, 0x00, 0x10, 0x28, 0x44, 
    0x00, 0x28, 0x28, 0x28, 0x00, 0x44, 0x28, 0x10, 0x00, 0x40, 
    0x54, 0x50, 0x20, 0x00, 0x38, 0x44, 0x5C, 0x54, 0x38, 0x00, 
    0x3C, 0x48, 0x48, 0x3C, 0x00, 0x7C, 0x54, 0x54, 0x28, 0x00, 
    0x38, 0x44, 0x44, 0x00, 0x7C, 0x44, 0x44, 0x38, 0x00, 0x7C, 
    0x54, 0x54, 0x00, 0x7C, 0x50, 0x50, 0x00, 0x38, 0x44, 0x54, 
    0x5C, 0x00, 0x7C, 0x10, 0x10, 0x7C, 0x00, 0x44, 0x7C, 0x44, 
    0x00, 0x08, 0x04, 0x44, 0x78, 0x00, 0x7C, 0x10, 0x28, 0x44, 
    0x00, 0x7C, 0x04, 0x04, 0x00, 0x7C, 0x20, 0x10, 0x20, 0x7C, 
    0x00, 0x7C, 0x20, 0x10, 0x7C, 0x00, 0x38, 0x44, 0x44, 0x38, 
    0x00, 0x7C, 0x48, 0x48, 0x30, 0x00, 0x38, 0x44, 0x44, 0x3A, 
    0x00, 0x7C, 0x48, 0x48, 0x34, 0x00, 0x24, 0x54, 0x54, 0x48, 
    0x00, 0x40, 0x7C, 0x40, 0x00, 0x78, 0x04, 0x04, 0x78, 0x00, 
    0x78, 0x04, 0x18, 0x60, 0x00, 0x78, 0x04, 0x38, 0x04, 0x78, 
    0x00, 0x6C, 0x10, 0x10, 0x6C, 0x00, 0x60, 0x14, 0x14, 0x78, 
    0x00, 0x4C, 0x54, 0x64, 0x00, 0x7C, 0x44, 0x00, 0x40, 0x20, 
    0x10, 0x08, 0x04, 0x00, 0x44, 0x7C, 0x00, 0x20, 0x40, 0x20, 
    0x00, 0x04, 0x04, 0x04, 0x04, 0x00, 0x40, 0x20, 0x00, 0x18, 
    0x24, 0x24, 0x3C, 0x00, 0x7C, 0x24, 0x24, 0x18, 0x00, 0x18, 
    0x24, 0x24, 0x00, 0x18, 0x24, 0x24, 0x7C, 0x00, 0x18, 0x2C, 
    0x34, 0x10, 0x00, 0x10, 0x3C, 0x50, 0x00, 0x18, 0x25, 0x25, 
    0x3E, 0x00, 0x7C, 0x20, 0x20, 0x1C, 0x00, 0x5C, 0x00, 0x01, 
    0x5E, 0x00, 0x7C, 0x08, 0x18, 0x24, 0x00, 0x7C, 0x00, 0x3C, 
    0x20, 0x3C, 0x20, 0x1C, 0x00, 0x3C, 0x20, 0x20, 0x1C, 0x00, 
    0x18, 0x24, 0x24, 0x18, 0x00, 0x3F, 0x24, 0x24, 0x18, 0x00, 
    0x18, 0x24, 0x24, 0x3F, 0x00, 0x3C, 0x10, 0x20, 0x00, 0x14, 
    0x34, 0x2C, 0x28, 0x00, 0x20, 0x78, 0x24, 0x00, 0x38, 0x04, 
    0x04, 0x3C, 0x00, 0x38, 0x04, 0x08, 0x30, 0x00, 0x30, 0x0C, 
    0x30, 0x0C, 0x30, 0x00, 0x24, 0x18, 0x24, 0x00, 0x38, 0x05, 
    0x05, 0x3E, 0x00, 0x24, 0x2C, 0x34, 0x24, 0x00, 0x10, 0x6C, 
    0x44, 0x00, 0x7C, 0x00, 0x44, 0x6C, 0x10, 0x00, 0x20, 0x40, 
    0x20, 0x40, 0x00, 0x7E, 0x42, 0x42, 0x7E, 0x00
};

unsigned char tiny_font_widths[] = {
    0x05, 0x02, 0x04, 0x06, 0x05, 0x06, 0x06, 0x02, 0x03, 
    0x03, 0x04, 0x04, 0x03, 0x04, 0x02, 0x06, 0x05, 0x03, 0x05, 
    0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x02, 0x02, 0x04, 
    0x04, 0x04, 0x05, 0x06, 0x05, 0x05, 0x04, 0x05, 0x04, 0x04, 
    0x05, 0x05, 0x04, 0x05, 0x05, 0x04, 0x06, 0x05, 0x05, 0x05, 
    0x05, 0x05, 0x05, 0x04, 0x05, 0x05, 0x06, 0x05, 0x05, 0x04, 
    0x03, 0x06, 0x03, 0x04, 0x05, 0x03, 0x05, 0x05, 0x04, 0x05, 
    0x05, 0x04, 0x05, 0x05, 0x02, 0x03, 0x05, 0x02, 0x06, 0x05, 
    0x05, 0x05, 0x05, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06, 0x04, 
    0x05, 0x05, 0x04, 0x02, 0x04, 0x05, 0x05, 0x00
 };

static const charset tiny_font_charset = {
    /*.bytes = */       tiny_font_bytes,
    /*.widths = */      tiny_font_widths,
    // /*.numchars = */    TINY_FONT_NUMCHARS,
    /*.first = */       TINY_FONT_FIRST,
    /*.nullchar = */    '~' + 1,
    /*.height = */      TINY_FONT_HEIGHT
};

#endif
