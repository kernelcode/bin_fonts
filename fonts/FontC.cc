/* 
 * FontC.cc
 * 
 *  Copyright (C) 2012 Brian Starkey
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * Font class and charset struct for storing binary fonts
 * 
 */

#include <numeric>
#include <list>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cctype>
#include "FontC.hh"

#define WRAP_THRESH 10

using namespace std;

list<string> &split(const string &s, char delim, list<string> &elems) {
    stringstream ss(s);
    string item;
    while(getline(ss, item, delim)) {
        if (item.size()) {
            elems.push_back(item);
        }
    }
    return elems;
}


list<string> split(const string &s, char delim) {
    list<string> elems;
    split(s, delim, elems);
    return elems;
}

FontC::FontC(const charset& set) : charset_(&set), numChars_(strlen((const char *)charset_->widths)), totalWidth_(accumulate(set.widths, set.widths + numChars_, 0)), last_(set.first + numChars_) {
    positions_ = new unsigned int[numChars_];
    positions_[0] = 0;
    for (int i(1); i < numChars_; i++) {
        positions_[i] = positions_[i-1] + charset_->widths[i-1];
        //cout << "positions_[" << i << "] = " << (int)positions_[i] << endl;
    }
};

FontC::~FontC() {
    delete[] positions_;
};
// Return index of first character
unsigned char FontC::getFirstChar( void ) {
    return charset_->first;
};

// Return number of characters in set
unsigned char FontC::getNumChars( void ) {
    return numChars_;
};

// Return height
unsigned char FontC::getHeight( void ) {
    return charset_->height;
};

// Return true if valid
int FontC::isValid( unsigned char index) {
        return ((index >= charset_->first) && (index < last_));
};

// Get width of specified character
unsigned char FontC::getWidth( unsigned char chr ) {
    if (isValid(chr)) {
        return charset_->widths[chr - charset_->first];
    } else {
        return getWidth(charset_->nullchar);
    }
};

// Get width of specified string
unsigned int FontC::getWidth( std::string &str ) {
    if (str.size())
        return getWidth((unsigned char *)str.c_str());
    return 0;
};

unsigned int FontC::getWidth( unsigned char * cstr ) {
    unsigned int total = 0, i = 0;
    
    while (cstr[i] != '\0') {
        total += getWidth(cstr[i++]);
    }
    
    return total;
};

unsigned char * FontC::getBase(unsigned char chr) {
    if (isValid(chr)) {
        return charset_->bytes + positions_[chr - charset_->first];
    } else {
        return getBase(charset_->nullchar);
    }
};

// Generate a bitmap of str
// Note that str need not be a single word, word just signifies no wrapping
BitmapC FontC::makeWord(std::string& str) {
    int width = getWidth(str);
    int height = getHeight();
    BitmapC bmp(width, height);
    
    //unsigned char * buffer = bmp.getBuf();
    
    string::iterator it;
    int offset(0);
    for ( it = str.begin(); it < str.end(); it++) {
        unsigned char c_width = getWidth(*it);
        unsigned char * c_base = getBase(*it);
        for (int i(0); i < height; i++) {
            unsigned int c_offset = i * totalWidth_;
            unsigned int b_offset = i * width;
            
            //memcpy(buffer + offset + b_offset, c_base + c_offset, c_width);
            //bmp.paintAt(offset + b_offset, (const char *)c_base + c_offset, c_width);
            bmp.paintAt(offset + b_offset, (const char *)c_base + c_offset, c_width);
            //cout << "offset + b_offset: " << (int)offset + b_offset << endl;
        }
        offset += c_width;
    }
    
    return bmp;
};

string splitWord(string &str, unsigned int width, FontC &font) {
    
    int subwidth(0), i(0), c_width(0), d_width = font.getWidth('-');
    string start;
    
    while ( (c_width = font.getWidth(str[i])), ((unsigned int)(subwidth += c_width) < width) ) {
        i++;
    };
    // str.substr(0, i) will fit
    
    // Do we hyphenate?
    if (isalpha(str[i - 1])) {
        // Yes. Where?
        width -= d_width;
        while (((unsigned int)(subwidth -= c_width) > width)) {
            i--;
            c_width = font.getWidth(str[i]);
        }
        start = str.substr(0, i + 1);
        start[i] = '-';
    } else { // No.
        start = str.substr(0, i);
    }
    
    str = str.substr(i); // Modify original string
    
    return start;

}

list<string> wrapText(string& sentence, unsigned int width, FontC &font) {
    
    list<string> tokens = split(sentence, ' ');
    list<string>::iterator it;
    
    unsigned int t_width, x_pos(0), end(0), space_w(font.getWidth(' '));
    
    for ( it=tokens.begin(); it != tokens.end(); it++) {
        t_width = font.getWidth(*it);
        end = (x_pos + t_width);

        if (end <= width) { // It fits
            
            if (x_pos > 0) { // If we aren't starting a new line
                list<string>::iterator t = it;
                
                // Add this word to the current line
                *(--it) += *t;
                // Remove the original copy
                tokens.erase(t);
            }
            
            x_pos += t_width; // Line now has this much more stuff on it
            
            if ((x_pos + space_w) < width) {// There is room for a space
                *it += ' ';
                x_pos += space_w;
            } else {                        // There isn't, we need to start a new line
                x_pos = 0;
            }
        
        } else { // It doesn't fit!
            
            if ( (width - x_pos) < WRAP_THRESH) {   // Only a small amount fits - start a new line
                it--;   // Force checking to see if it fits on a new line
            
            } else {    // We should split the word
                
                string start = splitWord(*it, width - x_pos, font); // start contains first part, *it contains end
            
                if (x_pos > 0) { // Add it to this line
                    *(--it) += start; // We know it fits
                
                } else { // It takes a whole new line
                    it = tokens.insert(it, start);
                }
            }
            x_pos = 0; // Now we have to start a new line
         }
    }
    
    return tokens;
}

