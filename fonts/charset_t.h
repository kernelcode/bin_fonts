#ifndef CHARSET_T_H
#define CHARSET_T_H

/*
 * charset_t.h
 * 
 * Copyright 2012 Brian Starkey <kernelcode@eva>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * Struct suited for storing binary fonts
 * 
 */

// Printable chars:  !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~

typedef struct charset_t {
    unsigned char * bytes;    // Pointer to character data
    unsigned char * widths;   // Pointer to widths array
//    unsigned char numchars;   // Number of characters
    unsigned char first;      // Char code of first character
    unsigned char nullchar;   // Index of null char  
    unsigned char height;     // Height of each character (in bytes)
} charset;

#endif
