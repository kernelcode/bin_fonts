#ifndef FONTC_HH
#define FONTC_HH

/* 
 * FontC.hh
 * 
 *  Copyright (C) 2012 Brian Starkey
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * Font class and charset struct for storing binary fonts
 * 
 */

#include <string>
//#include <vector>
#include <list>

#include "charset_t.h"
#include "BitmapC.hh"

class FontC {
  
  private:
    const charset * const charset_; // Pointer to charset class
    unsigned int * positions_;      // Base position of each character
    FontC(const FontC& font);       // No copies (no point)
    unsigned char numChars_;        // Number of characters in charset
    unsigned char totalWidth_;      // Cumulative width of all characters
    unsigned char last_;            // _charset->first + numChars_
    
    // Evaluates true if index is in the charset
    int isValid( unsigned char index);
    unsigned char * getBase(unsigned char chr);
    
    //Build vector for a character (index should be range checked)
    //std::vector<std::vector<unsigned char> > getVector( unsigned char index );
    

  public:
    FontC(const charset& set);
    ~FontC( void );
    
    unsigned char getFirstChar( void ); // Return index of first character
    unsigned char getNumChars( void );  // Return number of characters in set
    unsigned char getHeight( void );    // Return height
    unsigned char getWidth( unsigned char index );  // Get width of specified character
    unsigned int getWidth( std::string &msg );      // Get width of specified string
    unsigned int getWidth( unsigned char * cstr );
    BitmapC makeWord(std::string &str);
    //std::vector<std::vector<unsigned char> > getChar( unsigned char index );  // Return vector (length height) of strings representing character
  
};

std::list<std::string> &split(const std::string &s, char delim, std::list<std::string> &elems);

std::list<std::string> split(const std::string &s, char delim);

std::list<std::string> wrapText(std::string &sentence, unsigned int width, FontC &font);

#endif
