# Also used for "main".cc
PROG:=generate_text

# Defaults
CC:=gcc
CXX:=g++

CFLAGS:= -Wall -Werror -pg
CXXFLAGS:= -Wall -Werror -pg

ifeq ($(ARCH),arm)
	CC:=$(CROSS_COMPILE)$(CC)
	CXX:=$(CROSS_COMPILE)$(CXX)
endif

# Module Directories
MODULES:=fonts graphics

# Search for includes in each module dir
INC += $(MODULES)

CFLAGS += 
CXXFLAGS += 

# extra libraries if required
LIBS := 

# each module will add to this
SRC := $(PROG).cc

# include the description for
# each module
include $(patsubst %, %/module.mk,$(MODULES))

IDIRS = $(patsubst %, -I%, $(INC))

# determine the object files
COBJ := $(patsubst %.c,%.o, $(filter %.c,$(SRC)))
CXXOBJ := $(patsubst %.cc,%.o, $(filter %.cc,$(SRC)))
OBJ := $(COBJ) $(CXXOBJ)

.PHONY: all clean

all: $(SRC) $(PROG)

# link the program
$(PROG): $(OBJ)
	$(CXX) -pg -o $@ $(OBJ) $(LIBS)

$(COBJ): %.o : %.c
	$(CC) -c $(CFLAGS) $(IDIRS) $< -o $@
	$(CC) -MM $(CFLAGS) $(IDIRS) $*.c > $*.d

$(CXXOBJ): %.o : %.cc
	$(CXX) -c $(CXXFLAGS) $(IDIRS) $< -o $@
	$(CXX) -MM $(CFLAGS) $(IDIRS) $*.cc > $*.d

# include the C include
# dependencies
-include $(OBJ:.o=.d)

clean:
	rm -rf $(OBJ)
	rm $(PROG)

