/*
 * font_test.cc
 * 
 * Copyright 2012 Brian Starkey <stark3y@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * Test harness for FontC
 * 
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <list>

#include "fonts/FontC.hh"
#include "fonts/tiny_font.h"

#define LINE_WIDTH 101
#define NUM_LINES 5

using namespace std;

int main( int argc, char * argv[]) {
    
    if (argc != 2) {
        cerr << "Takes exactly one argument - a string to print" << endl;
        cerr << "Outputs a binary stream representing the input string" << endl;
        return 1;
    }
    
    FontC tinyFont(tiny_font_charset);
    string msg(argv[1]);
    
    // wrapText returns a list of strings, splitting msg into segments
    // which fit into LINE_WIDTH when using tinyFont
    list<string> spl = wrapText( msg, LINE_WIDTH, tinyFont);
    list<string>::iterator it;
    
    for ( it = spl.begin(); it != spl.end(); it++) {
        
        BitmapC word(tinyFont.makeWord(*it));
        int i( word.getWidth() );
        
        fwrite (word.getBuf(0), 1, i, stdout );

        while (i < LINE_WIDTH) { // Pad to a full line
            fwrite("\0", 1, 1, stdout);
            i++;
        }
    }
    
    return 0;
}
