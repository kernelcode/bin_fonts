#ifndef SPRITEC_CC
#define SPRITEC_CC

/*
 * SpriteC.cc
 * 
 * Copyright 2012 Brian Starkey <kernelcode@eva>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "SpriteC.hh"


SpriteC::SpriteC(int x, int y, int z, BitmapC &bmp, void (*callback)(SpriteC &), int life, draw_mode mode, bool wrap) : BitmapC(bmp), x_(x), y_(y), z_(z), callback_(callback), life_(life), mode_(mode), wrap_(wrap) {
    right_ = (x_ + width_);
    bottom_ = (y_ + height_);
}

void SpriteC::setX(int x) {
    if ( x < SP_MIN_VX ) {
        x_ = SP_MIN_VX;
    } else if ( x > SP_MAX_VX ) {
        x_ = SP_MAX_VX;
    } else {
        x_ = x;
    }
    right_ = x_ + width_;
}

void SpriteC::setY(int y) {
    if ( y < SP_MIN_VY ) {
        y_ = SP_MIN_VY;
    } else if ( y > SP_MAX_VY ) {
        y_ = SP_MAX_VY;
    } else {
        y_ = y;
    }
    bottom_ = y_ + height_;
}

void SpriteC::setZ(int z) {
    z_ = z;
}

void SpriteC::setWrap(bool wrap) {
    wrap_ = wrap;
}

int SpriteC::getX() {
    return x_;
}

int SpriteC::getY() {
    return y_;
}

int SpriteC::getZ() {
    return z_;
}

bool SpriteC::getWrap() {
    return wrap_;
}

int SpriteC::getBottom() {
    return bottom_;
}

int SpriteC::getRight() {
    return right_;
}

void SpriteC::callback( void ) {
    if (callback_) {
        callback_(*this);
    };
}

void SpriteC::setMode( draw_mode mode ) {
    mode_ = mode;
};

draw_mode SpriteC::getMode( void ) {
    return mode_;
};

#endif

