#ifndef BITMAPC_HH
#define BITMAPC_HH

/*
 * BitmapC.hh
 * 
 * Copyright 2012 Brian Starkey <kernelcode@eva>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <string>

typedef enum draw_mode {
     DMODE_EQ     = 0,
     DMODE_OR     = (1 << 0),
     DMODE_AND    = (1 << 1),
     DMODE_ANDNOT = (1 << 2),
     DMODE_XOR    = (1 << 3)
} draw_mode;

class BitmapC {
  
  private:
    unsigned char * data_;
    unsigned long int size_;
    bool scroll_;
    bool stretchy_;
    bool mmapped_;
  
  protected:
    unsigned short width_;
    unsigned short height_;
    
  public:
    BitmapC();
    ~BitmapC();
    BitmapC(const BitmapC& copy);
    BitmapC(int fd, unsigned int width, unsigned int height); // Memory map
    BitmapC(const unsigned char * data, unsigned int width, bool stretchy = false);
    BitmapC(const unsigned char * data, unsigned int width, unsigned int height, bool stretchy = false);
    BitmapC(unsigned int width, unsigned int height, bool stretchy = false);
    BitmapC(const std::string& file);
    
    unsigned int getHeight();
    unsigned int getWidth();
    unsigned int getSize();
    int paintAt(int x, int y, const BitmapC &bmp, draw_mode mode = DMODE_EQ);
    int paintAt(unsigned int offset, const char * data, unsigned int size, draw_mode mode = DMODE_EQ);
    const unsigned char * getBuf(unsigned int i) const;
    const unsigned char * getRow(unsigned int i) const;
    
    void fill(unsigned char value);
    void clear( void );
    
    BitmapC& operator=(const BitmapC &b);
    
};


#endif
