/*
 * BitmapC.hh
 * 
 * Copyright 2012 Brian Starkey <kernelcode@eva>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sys/mman.h>
#include <bmpfile.h>

#include "BitmapC.hh"

using namespace std;

BitmapC::BitmapC() : scroll_(0), width_(0), height_(0) {

};

BitmapC::~BitmapC() {
    if (mmapped_) {
        munmap(data_, size_);
    } else {
        delete[] data_;
    }
};

// Memory map
BitmapC::BitmapC(int fd, unsigned int width, unsigned int height) : size_(width * height), scroll_(false), stretchy_(false), mmapped_(false), width_(width), height_(height) {
    data_ = (unsigned char *)mmap(0, size_, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (data_ == MAP_FAILED) {
        throw std::runtime_error("BitmapC Unable to map");
    }
    mmapped_ = true;
};

BitmapC::BitmapC(const BitmapC& copy) : size_(copy.width_ * copy.height_), scroll_(copy.scroll_), stretchy_(copy.stretchy_), mmapped_(false), width_(copy.width_), height_(copy.height_) {
    data_ = new unsigned char[size_];
    memcpy(data_, copy.data_, size_);
};

BitmapC::BitmapC(const unsigned char * data, unsigned int width, bool stretchy) : size_(width), scroll_(false), stretchy_(stretchy), mmapped_(false), width_(width), height_(1) {
    data_ = new unsigned char[size_];
    memcpy(data_, data, size_);
};

BitmapC::BitmapC(const unsigned char * data, unsigned int width, unsigned int height, bool stretchy) : size_(width * height), scroll_(false), stretchy_(stretchy), mmapped_(false), width_(width), height_(height) {
    data_ = new unsigned char[size_];
    memcpy(data_, data, size_);
};

BitmapC::BitmapC(unsigned int width, unsigned int height, bool stretchy) : size_(width * height), scroll_(false), stretchy_(stretchy), mmapped_(false), width_(width), height_(height) {
    data_ = new unsigned char[size_];
    memset(data_, 0x00, size_);
};

BitmapC::BitmapC(const std::string& file) : scroll_(false), stretchy_(false) {
    bmpfile_t * bmp, * baw;
    
    bmp = bmp_create_from_file(file.c_str());
    if (!bmp) {
        cerr << "Could not create bitmap from file " << file << endl;
        return;
    }
    
    width_ = bmp_get_width(bmp);
    height_ = bmp_get_height(bmp);
    size_ = width_ * height_;
    data_ = new unsigned char[size_];
    memset(data_, 0x00, size_);
    
    baw = bmp_create(1, 1, 1); // Just used for getting closest colour
    
    if (!baw) {
        cerr << "Could not create conversion bmp" << endl;
        return;
    }
    
    int offset(0);
    for (int y(0); y < height_; y += 8, offset += width_) {
        for (int x(0); x < width_; x++) {
            int bit = 0;
            uint8_t byte = 0;
            do {
                rgb_pixel_t * pix = bmp_get_pixel(bmp, x, y + bit);
                byte |= bmp_find_closest_color(baw, *pix) << (7 - bit);
                bit++;
            } while (bit % 8);
            data_[offset + x] = ~byte;
        }
    }
    
    bmp_destroy(bmp);
    bmp_destroy(baw);

};

unsigned int BitmapC::getHeight() {
    return height_;
};

unsigned int BitmapC::getWidth() {
    return width_;
};

unsigned int BitmapC::getSize() {
    return size_;
};

int BitmapC::paintAt(int x, int y, const BitmapC &bmp, draw_mode mode) {
    int ret(0);
    
    int bmp_right = x + bmp.width_;
    int bmp_bottom = y + bmp.height_;    
    
    if ((x < 0) || (y < 0) || (bmp_right > width_) || (bmp_bottom > height_)) {
        if (stretchy_) {
            int min_x = x < 0 ? x : 0;
            int min_y = y < 0 ? y : 0;
            int max_x = bmp_right > width_ ? bmp_right : width_;
            int max_y = bmp_bottom > height_ ? bmp_bottom : height_;
            BitmapC t_bmp((unsigned int)(max_x - min_x), (unsigned int)(max_y - min_y), stretchy_);
            t_bmp.paintAt((0 - min_x), (0 - min_y), *this);
            t_bmp.paintAt((x - min_x), (y - min_y), *this);
            *this = t_bmp;
            return 1; // We stretched to fit
        } else {
            ret = -1; // We clipped
        }
    }
    if ((x > width_) || (y > height_) || (bmp_right < 0) || (bmp_bottom < 0)) return ret; //Nothing to do!
    
    int min_x = x < 0 ? 0 : x;
    int min_y = y < 0 ? 0 : y;
    int lim_x = bmp_right < width_ ? bmp_right : width_;
    int lim_y = bmp_bottom < height_ ? bmp_bottom : height_;
    unsigned char * source_base = bmp.data_ + min_x - x;
    int num_x = lim_x - min_x;
    //cout << "x: " << x << ", y: " << y << ", min_x: " << min_x << ", min_y: " << min_y << ", lim_x: " << lim_x << ", lim_y: " << lim_y << endl;
    for (int i(min_y); i < lim_y; i++) { // For each y position on screen
        int source_y = i - y;
        unsigned int dest_offset = ( i * width_ ) + min_x;
        paintAt(dest_offset, (const char *)(source_base + ( source_y * bmp.width_ )), num_x, mode);
    }

    return ret;
};

/*
 * int BitmapC::paintAt(unsigned int x, unsigned int y, const BitmapC &bmp, draw_mode mode) {
    unsigned int x_tot = x + bmp.width_;
    unsigned int y_tot = y + bmp.height_;
    
    int ret(0);
    
    // Stretch to fit
    if ((x_tot > width_) || (y_tot > height_)) {
        if (stretchy_) {
            unsigned int maxx = x_tot > width_ ? x_tot : width_;
            unsigned int maxy = y_tot > height_ ? y_tot : height_;
            BitmapC t_bmp(maxx, maxy, stretchy_);
            t_bmp.paintAt(0, 0, *this);
            t_bmp.paintAt(x, y, bmp);
            *this = t_bmp;
            return 1;
        } else {
            ret = -1; // Will have to clip
        }
    }
    // Paint
    unsigned int lim_x = (x_tot < width_) ? x_tot : width_;
    unsigned int lim_y = (y_tot < height_) ? y_tot : height_;
    unsigned int num_x = lim_x - x;
    unsigned int num_y = lim_y - y;
    if (num_x < 0) num_x = 0;
    if (num_y < 0) num_y = 0;
    
    unsigned char * source;
    unsigned int dest_offset;
    for (unsigned int i(0); i < num_y; i++) {
        source = bmp.data_ + (i * bmp.width_);
        dest_offset = (x + ((y + i) * width_));
        paintAt(dest_offset, (const char *)source, num_x, mode);
    }
   
    return ret;
};
*/

int BitmapC::paintAt(unsigned int offset, const char * data, unsigned int size, draw_mode mode) {
    int ret(0);
    if (offset + size > size_) {
        size = size_;
        ret = -1;
    }
    unsigned char * dest = data_ + offset;
    
    if (mode == DMODE_EQ) { // Straight copy
        memcpy(dest, data, size);
    } else if (mode == DMODE_OR) { // Set only
        for (unsigned int j(0); j < size; j++) {
            *(dest + j) |= *(data + j);
        }
    } else if (mode == DMODE_AND) { // AND pixels
        for (unsigned int j(0); j < size; j++) {
            *(dest + j) &= *(data + j);
        }
    } else if (mode == DMODE_ANDNOT) { // Clear only
        for (unsigned int j(0); j < size; j++) {
            *(dest + j) &= ~*(data + j);
        }
    } else if (mode == DMODE_XOR) { // XOR pixels
        for (unsigned int j(0); j < size; j++) {
            *(dest + j) ^= *(data + j);
        }
    }
    
    return ret;
};

BitmapC& BitmapC::operator=(const BitmapC &b) {
    if ((&b != this) && (data_ != NULL)) {
		if (mmapped_) {
            munmap(data_, size_);
            mmapped_ = false;
        } else {
            delete[] data_;
        }
		
        data_ = new unsigned char[b.size_];
        width_ = b.width_;
        height_ = b.height_;
        size_ = b.size_;
        scroll_ = b.scroll_;
        stretchy_ = b.stretchy_;
        
		memcpy(data_, b.data_, size_);
	}
	
	return *this;
}

const unsigned char * BitmapC::getRow(unsigned int i) const {
    if (i > height_) return NULL;
    return &data_[i * width_];
};

const unsigned char * BitmapC::getBuf(unsigned int i) const {
    if (i > size_) return NULL;
    return &data_[i];
};

void BitmapC::fill(unsigned char value) {
    memset(data_, value, size_);
};

void BitmapC::clear( void ) {
    fill(0x00);
}

