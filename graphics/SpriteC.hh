#ifndef SPRITEC_HH
#define SPRITEC_HH

/*
 * SpriteC.hh
 * 
 * Copyright 2012 Brian Starkey <kernelcode@eva>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include "BitmapC.hh"

#define SP_MAX_VX 250
#define SP_MIN_VX -250
#define SP_MAX_VY 15
#define SP_MIN_VY -15

class SpriteC : public BitmapC {
  
  private:
    int x_, y_, z_;
    void (*callback_)(SpriteC &sprite);
    int life_;
    int bottom_, right_;
    draw_mode mode_;
    bool wrap_;
    
  public:
    SpriteC(int x, int y, int z, BitmapC &bmp, void (*callback)(SpriteC &), int life = -1, draw_mode = DMODE_EQ, bool wrap = false);
    //const void * handle; // Set at instantiation. Unchangeable.
    
    void setX(int x);
    void setY(int y);
    void setZ(int z);
    void setWrap(bool wrap);
    int getX( void );
    int getY( void );
    int getZ( void );
    bool getWrap( void );
    int getBottom( void );
    int getRight( void );
    void setMode( draw_mode mode );
    draw_mode getMode( void );
    void callback( void );
    

};


#endif

